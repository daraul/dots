;; -*- mode: elisp -*-

;; Disable the splash screen (to enable it agin, replace the t with 0)
(setq inhibit-splash-screen t)

;; Enable transient mark mode
(transient-mark-mode 1)

;;;;Org mode configuration
(setq org-todo-keywords
  '((sequence "TODO(t!)" "NEXT" "IN-PROGRESS" "WAITING" "|" "IGNORED(c@)" "DONE(d@)" "|" "FAILED(f@)")))
(setq org-log-done 'time)
;; Enable Org mode
(require 'org-edna)
(org-edna-mode)
(require 'org-id)
(setq org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)
(global-set-key "\C-cl" 'org-id-store-link)
;; Make Org mode work with files ending in .org
;; The above is the default in recent emacsen
;; Enable extra todo keywords
;; Enable todo depdencies
;; Enable C-c a for org-agenda
(global-set-key "\C-ca" 'org-agenda)

(setq org-agenda-files '("~/.task/1.org" "~/.task/completed.org" "~/.task/notes"))
(setq org-agenda-custom-commands
      '(("r" "Reading list" tags-todo "reading_list") ;; My reading list
        ("w" "Watch list" tags-todo "to_watch") ;; My watch list
        ))
(setq org-refile-use-outline-path 'file)
(setq org-refile-targets '((org-agenda-files :maxlevel . 3)))
(setq org-log-into-drawer "LOGBOOK")

;; Setup org-protocol
(server-start)
(require 'org-roam-protocol)

(setq org-roam-capture-templates
 '(("r" "default" plain (function org-roam--capture-get-point)
     "%?"
     :file-name "%<%Y%m%d%H%M%S>-${slug}"
     :head "#+TITLE: ${title}\n#+CREATED: %<%Y-%m-%d %H:%M:%S>"
     :unnarrowed t)))

(setq org-roam-capture-ref-templates
 '(("r" "default" plain (function org-roam--capture-get-point)
     "%?"
     :file-name "%<%Y%m%d%H%M%S>-${slug}"
     :head "#+TITLE: ${title}\n#+ROAM_KEY: ${ref}\n#+CREATED: %<%Y-%m-%d %H:%M:%S>\n\n${ref}"
     :unnarrowed t)))

;; Enable evil mode
(require 'evil)
(evil-mode 1)

;; Enable ledger mode
(require 'ledger-mode)

(global-display-line-numbers-mode)

(setq org-roam-directory "~/.task/notes")
(add-hook 'after-init-hook 'org-roam-mode)
(global-set-key "\C-cf" 'org-roam-find-file)
(global-set-key "\C-cr" 'org-roam-find-ref)
(define-key minibuffer-local-completion-map (kbd "SPC") 'self-insert-command)
(require 'git-gutter)

(global-git-gutter-mode +1)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((js . t)))
(setq org-babel-js-function-wrapper
      "console.log(require('util').inspect(function(){\n%s\n}(), { depth: 100 }))")

(add-hook 'ledger-mode-hook
	  (lambda ()
	    (setq-local tab-always-indent 'complete)
	    (setq-local completion-cycle-threshold t)
	    (setq-local ledger-complete-in-steps t)))

