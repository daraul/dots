# Managed using [home-manager](https://github.com/rycee/home-manager)

## Installing "packages"
I am transitioning from nix to stow for installation of my dotfiles (or "packages" as stow would call them). The only one moved so far has been my gpg-agent.conf file. To install it run this:

``` sh
stow -t ~/.gnupg .gnupg
```

That should symlink the files in to ./.gnupg folder to ~/.gnupg. 

## Getting setup

- Create secrets.nix:
```nix
{
  gmail: "whatever";
  intellectstorm: "whatever";
}
```
- Install home-manager: https://rycee.gitlab.io/home-manager/index.html#ch-installation
- Once installed, clone this repository into `~/.config/nixpkgs`
- (optional) backup anything you want to keep, as these files/folders must be replaced:
  - ~/.bashrc
  - ~/.profile
  - ~/.config/
    - awesome
    - rofi
  - ~/.vim/
- Now run `home-manager switch` to set everything up

You should be good to go.
