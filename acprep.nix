pkgs: attrs:
  with pkgs;
    let defaultAttrs = {
        builder = "${bash}/bin/bash";
        args = [ ./acprep.sh ];
        baseInputs = [ gnutar gzip gnumake boost tzdata ];
        buildInputs = [];
        system = builtins.currentSystem;
    };
  in
  derivation (defaultAttrs // attrs)
