with import <nixpkgs> {};

{ config, pkgs, fonts, ... }:

let
  secrets = import ./secrets.nix;
in
{
  programs.home-manager.enable = true; # Let home-manager manage itself
  fonts.fontconfig.enable = true;
  
  home.stateVersion = "20.03";

  home.packages = with pkgs; [
    w3m
    zathura
    texlive.combined.scheme-full
    deluge
    nodejs
    libpng
    zlib
    poppler
    imagemagick
    isync
    mu
    ledger
    xorg.setxkbmap
    spotify
    paperkey
    pywal
    wmctrl
    xorg.xprop
    dzen2
    slop
    symbola
    unifont
    siji
    sxhkd
    qrencode
    ripgrep
    nodePackages.vue-language-server
    playerctl
    recutils
  ];
  nixpkgs.config.allowUnfree = true;

  programs.tmux = { # Stolen from https://github.com/srid/nix-config/blob/19658553f1d5c07cb7ff7abc2fe3894289d355cd/nix/home/tmux.nix
    enable = true;
    shortcut = "a"; # Use Ctrl-a
    baseIndex = 1; # Widows numbers begin with 1
    keyMode = "vi";
  };

  home.file = {
    ".profile".source = ./.profile;
    ".xinitrc".source = ./.xinitrc;
    "Applications/serenata.phar".source = fetchurl {
      "url" = "https://gitlab.com/Serenata/Serenata/uploads/273c7ff8b52dde588168215f64065485/distribution-7.4.phar";
      "sha256" = "0glv4glfw9pn88kbjnb0mp0b9ady758rdnqzkc08rpgpq7xrdjnf";
    };
    ".share/applications/emacs.desktop".source = ./emacs.desktop;
    ".share/applications/org-protocol.desktop".source = ./org-protocol.desktop;
    ".mbsyncrc".text = ''
                     IMAPAccount thurst306@gmail.com
                     Host imap.gmail.com
                     User thurst306@gmail.com
                     Pass "${secrets.gmail}"
                     SSLType IMAPS
                     CertificateFile /etc/ssl/certs/ca-certificates.crt

                     IMAPAccount intellectstorm
                     Host casper.svldns.com
                     User daraul@intellectstorm.com
                     Pass "${secrets.intellectstorm}"
                     Port 993
                     SSLType IMAPS
                     AuthMechs Login
                     CertificateFile /etc/ssl/certs/ca-certificates.crt

                     IMAPStore daraul@intellectstorm.com-remote
                     Account intellectstorm

                     IMAPStore thurst306@gmail.com-remote
                     Account thurst306@gmail.com

                     MaildirStore daraul@intellectstorm.com-local
                     Path ~/.mail/mbsyncmail/
                     Inbox ~/.mail/mbsyncmail/INBOX
                     SubFolders Verbatim

                     MaildirStore thurst306@gmail.com-local
                     Path ~/.mail/thurst306@gmail.com/
                     Inbox ~/.mail/thurst306@gmail.com/INBOX
                     SubFolders Verbatim

                     Channel daraul@intellectstorm.com
                     Master :daraul@intellectstorm.com-remote:
                     Slave :daraul@intellectstorm.com-local:
                     Patterns "Sent" "INBOX" "Deleted Items"
                     Create Slave
                     Sync All
                     MaxMessages 100
                     Expunge Both
                     SyncState *

                     Channel daraul@intellectstorm.com-sent
                     Master :daraul@intellectstorm.com-remote:"Sent"
                     Slave :daraul@intellectstorm.com-local:"Sent Items"
                     Create Slave
                     MaxMessages 100
                     Expunge Both
                     SyncState *

                     Channel thurst306@gmail.com-inbox
                     Master :thurst306@gmail.com-remote:
                     Slave :thurst306@gmail.com-local:
                     Patterns "INBOX"
                     Create Slave
                     Sync All
                     MaxMessages 100
                     Expunge Both
                     SyncState *

                     Channel thurst306@gmail.com-sent
                     Master :thurst306@gmail.com-remote:"[Gmail]/Sent Mail"
                     Slave :thurst306@gmail.com-local:"Sent Items"
                     Create Slave
                     Sync All
                     MaxMessages 100
                     Expunge Both
                     SyncState *

                     Channel thurst306@gmail.com-drafts
                     Master :thurst306@gmail.com-remote:"[Gmail]/Drafts"
                     Slave :thurst306@gmail.com-local:"Drafts"
                     Create Slave
                     Sync All
                     MaxMessages 100
                     Expunge Both
                     SyncState *

                     Group thurst306@gmail.com
                     Channel thurst306@gmail.com-inbox
                     Channel thurst306@gmail.com-sent
                     Channel thurst306@gmail.com-drafts

                     Group daraul@intellectstorm.com
                     Channel daraul@intellectstorm.com
                     Channel daraul@intellectstorm.com-sent
    '';
    ".authinfo".text = ''
      machine casper.svldns.com login daraul@intellectstorm.com port 587 password ${secrets.intellectstorm}
      machine smtp.gmail.com login thurst306@gmail.com port 587 password ${secrets.gmail}
    '';
  };
}
