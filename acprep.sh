set -e
unset PATH
for p in $buildInputs $baseInputs; do
  export PATH=$p/bin${PATH:+:}$PATH
done

make
make install
