PATH="$HOME/.node_modules/bin:$PATH"
PATH="$HOME/.nvm/versions/node/v10.15.3/bin:$PATH"
PATH="$PATH:$HOME/.gem/ruby/2.6.0/bin"
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk
export ANDROID_HOME="$HOME/Android/Sdk"
export PATH="$PATH:$ANDROID_HOME/tools"
export PATH="$PATH:$ANDROID_HOME/tools/bin"
export PATH="$PATH:$ANDROID_HOME/build-tools/28.0.3"
export PATH="$PATH:$ANDROID_HOME/platform-tools"

