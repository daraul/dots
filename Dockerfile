FROM ubuntu:19.04

WORKDIR /root

RUN apt-get update && \
    apt-get install ansible sudo --yes

ARG PASSWORD=$6$xyz$.93lutCQh7gdmqGUmc0pqsyIV6oV.yg/JdutjxPyVxVRhfQnlOaWqbPLlVr.r.CkGAEbx50p7atJLgFxm972z0

# docker build -t dots --build-arg PASSWORD=$(openssl passwd -6 -salt xyz se) . && docker run -it -v $PWD/setup.yaml:/home/daraul/setup.yaml -w /home/daraul dots bash
RUN useradd -p $PASSWORD -U -m -d /home/daraul daraul

RUN usermod -aG sudo daraul

USER daraul

WORKDIR /home/daraul

COPY --chown=daraul:daraul id_rsa .ssh/id_rsa

COPY --chown=daraul:daraul setup.yaml original.yaml

