DEPENDENCIES = vim bashrc

final: $(DEPENDENCIES)
	home-manager switch

vim:
	make -C .vim setup

bashrc:
	test -f $(HOME)/.bashrc && echo '$(HOME)/.bashrc already exists' || ln -s $(CURDIR)/.bashrc $(HOME)/.bashrc
