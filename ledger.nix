let
    pkgs = import <nixpkgs> {};
    mkDerivation = import ./acprep.nix pkgs;
in mkDerivation {
    name = "ledger";
    src = pkgs.fetchFromGitHub {
      owner  = "ledger";
      repo   = "ledger";
      rev    = "v3.2.1";
      sha256 = "0x6jxwss3wwzbzlwmnwb8yzjk8f9wfawif4f1b74z2qg6hc4r7f6";
    };
}
