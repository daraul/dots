#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export APPMENU_DISPLAY_BOTH=1
if [ -n "$GTK_MODULES" ]
then
  GTK_MODULES="$GTK_MODULES:unity-gtk-module"
else
  GTK_MODULES="unity-gtk-module"
fi

if [ -z "$UBUNTU_MENUPROXY" ]
then
  UBUNTU_MENUPROXY=1
fi

export GTK_MODULES
export UBUNTU_MENUPROXY

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[ -f /home/daraul/.node_modules/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.bash ] && . /home/daraul/.node_modules/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.bash
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[ -f /home/daraul/.node_modules/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.bash ] && . /home/daraul/.node_modules/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.bash

export CHROME_BIN=$(which chromium)

if [ -x "$( command -v gopass )" ]; then source <(gopass completion bash); fi
if [ -x "$( command -v kubectl )" ]; then source <(kubectl completion bash); fi
if [ -x "$( command -v kops )" ]; then source <(kops completion bash); fi
export EDITOR="emacsclient -c"
export GIT_EDITOR="emacsclient -c"
if [ -f /etc/bash_completion ]; then source /etc/bash_completion; fi

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.emacs.d/bin"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
export ORG_FILE="$HOME/.task/1.org"

alias dots='/usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME'
alias gitl='/usr/bin/git --git-dir=$HOME/ledger/.git/ --work-tree=$HOME/ledger'
alias gitt='/usr/bin/git --git-dir=$HOME/.task/.git/ --work-tree=$HOME/.task'
alias gittw='/usr/bin/git --git-dir=$HOME/.timewarrior/.git/ --work-tree=$HOME/.timewarrior'
alias org='cat $ORG_FILE | grep TODO'
alias erg='emacs $ORG_FILE'
export LEDGER_FILE=$HOME/ledger/master.ledger
export TERMINFO_DIRS=/lib/terminfo

# Stolen from https://emacsredux.com/blog/2020/07/16/running-emacs-with-systemd/
export ALTERNATE_EDITOR=''
alias e='emacsclient -t'
alias yt2mp3='youtube-dl -x --audio-format mp3 --prefer-ffmpeg --add-metadata'

# export DOOMDIR="$HOME/.config/nixpkgs/emacs/doom.d"
# export EMACS="/usr/bin/flatpak run org.gnu.emacs"

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# if tmux is executable, X is running, and not inside a tmux session, then try to attach.
# if attachment fails, start a new session
# See https://wiki.archlinux.org/title/Tmux#Start_tmux_on_every_shell_login
if [ -x "$(command -v tmux)" ] && [ -n "${DISPLAY}" ]; then
  [ -z "${TMUX}" ] && { tmux attach -t main || tmux new -t main; } >/dev/null 2>&1
fi

set -o vi
